const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PhotoAuthorSchema = new Schema({
    title: {
        type: Schema.Types.ObjectId,
        ref: 'Photo',
        required: true
    },

    image: {
        type: Schema.Types.ObjectId,
        ref: 'Photo',
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

});

const PhotoAuthor = mongoose.model('PhotoAuthor', PhotoAuthorSchema);

module.exports = PhotoAuthor;