const mongoose = require('mongoose');
const config = require('./config');
const Photo = require('./models/Photo');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;
const collections = ['photos',  'users'];

db.once('open', async () => {

    collections.forEach(async () => {
        try {
            await db.dropDatabase();
        } catch (e) {
            console.log(`Collection did not exist in DB`);
        }
    });

    const [user, admin] =await User.create({
        username: 'user',
        password: '123',
        role: 'user'
    }, {
        username: 'admin',
        password: 'admin123',
        role: 'admin'
    });

    await Photo.create({
        title: 'cvxbcv',
        user: user._id,
        image: 'singer.jpg'
    }, {
        title: 'gfhg',
        planet: admin._id,
        image: 'singer.jpg'
    });


    db.close();

});
