const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, '/public/uploads'),
    db: {
        url: 'mongodb://localhost:27017',
        name: 'photos'
    },
    jwt:{
        secret: 'some kind secret',
        expiresIn: '7d'
    },
    facebook: {
        appId: "187220631985026",
        appSecret: "2ca63bb796cbb2c881b08f62b8e7108e"
    }
};