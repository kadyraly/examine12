const express = require('express');
const Photo = require('../models/Photo');
const nanoid = require('nanoid');
const multer = require('multer');
const auth = require('../middleware/auth');
const config = require("../config");
const path = require('path');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {

        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = (db) => {
    // Planet index
    router.get('/', (req, res) => {
        Photo.find()
            .populate('user')
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.get('/:id', (req, res) => {

        Photo.find({user: req.params.id})
            .populate('user')
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });



    router.post('/', [auth, upload.single('image')], (req, res) => {

        const photoData = req.body;

        if (req.file) {
            photoData.image = req.file.filename;
        } else {
            photoData.image = null;
        }

        req.body.user = req.user._id;

        const photo = new Photo(photoData);
        photo.save()
            .then(result => res.send(result))
            .catch((err) => {
                res.sendStatus(500)
            });
    });

    router.delete('/:id', async (req, res) => {
        console.log(req.params.id)
        let photoAuthor = await Photo.findByIdAndRemove(req.params.id);

        res.send(photoAuthor);
    });



    return router;
};

module.exports = createRouter;