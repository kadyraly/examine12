const express = require('express');
const PhotoAuthor = require('../models/PhotoAuthor');
const nanoid = require('nanoid');
const multer = require('multer');
const auth = require('../middleware/auth');
const config = require("../config");
const path = require('path');
const photos = require('./photos');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {

        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = (db) => {
    // Planet index
    router.get('/', (req, res) => {
        console.log(req.query.photos)
        if (req.query.photos) {
            PhotoAuthor.find({_id: req.query.photos}).populate('photos user')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.get('/:id', (req, res) => {
        PhotoAuthor
            .findOne({_id: req.params.id})
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404)
            })
            .catch(() => res.sendStatus(500));
    });


    router.post('/', [auth, upload.single('image')], (req, res) => {

        const photoAuthorData = req.body;

        if (req.file) {
            photoAuthorData.image = req.file.filename;
        } else {
            photoAuthorData.image = null;
        }

        req.body.user = req.user._id;

        const photoAuthor = new PhotoAuthor(photoAuthorData);
        photoAuthor.save()
            .then(result => res.send(result))
            .catch((err) => {
                res.sendStatus(500)
            });
    });


    router.delete('/:id', async (req, res) => {
console.log(req.params.id)
        let photoAuthor = await PhotoAuthor.findByIdAndRemove(req.params.id);

        res.send(photoAuthor);
    });



    return router;
};

module.exports = createRouter;