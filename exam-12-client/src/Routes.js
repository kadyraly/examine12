import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {Route, Switch} from "react-router-dom";

import React from "react";
import Photo from "./containers/Photo/Photo";
import NewPhoto from "./containers/NewPhoto/NewPhoto";
import PhotoAuthor from "./containers/PhotoAuthor/PhotoAuthor";

const Routes = () => (
    <Switch>
        <Route path="/" exact component={Photo} />
        <Route path="/photos/new" exact component={NewPhoto}/>
        <Route path="/photosAuthor/:id" exact component={PhotoAuthor}/>
        <Route path="/register" exact component={Register} />
        <Route path="/login" exact component={Login} />
    </Switch>

);




export default Routes;