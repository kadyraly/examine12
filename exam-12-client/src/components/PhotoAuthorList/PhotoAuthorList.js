import React, {Fragment} from 'react';
import {Button, Image, Panel} from "react-bootstrap";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import config from '../../config';
import notFound from '../../assets/image/not-found.png';


const PhotoAuthorList = props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + '/uploads/' + props.image;
    }
    return (
        <Panel>
            <Panel.Body>
                <Image
                    style={{width: '100px', marginRight: '10px'}}
                    src={image}
                    thumbnail
                /> <br/>
                <h3><Link to={'/popup'}>{props.title}</Link></h3> <br/>
                <Fragment >
                     <Button className="pull-right" onClick={props.click} bsStyle="primary" >Delete</Button>
                </Fragment>
            </Panel.Body>

        </Panel>
    );
};

PhotoAuthorList.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired
};

export default PhotoAuthorList;