import React from 'react';
import {Image, Panel} from "react-bootstrap";
import PropTypes from 'prop-types';

import config from '../../config';
import notFound from '../../assets/image/not-found.png';
import {Link} from "react-router-dom";

const PhotoList= props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + '/uploads/' + props.image;
    }

    return (
        <Panel>
            <Panel.Body>
                <Image
                    style={{width: '100px', marginRight: '10px'}}
                    src={image}
                    thumbnail
                /> <br/>
                <h3><Link to={'/popup'}>{props.title}</Link></h3> <br/>
                <b>By: </b><Link to={'/photosAuthor/' + props.userId}>{props.user}</Link>
            </Panel.Body>

        </Panel>
    );
};

PhotoList.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired
};

export default PhotoList;