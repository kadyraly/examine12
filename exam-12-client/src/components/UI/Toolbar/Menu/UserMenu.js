import React, {Fragment} from "react";
import {MenuItem, Nav, NavDropdown, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const UserName = ({user, logout}) => {
    const navTitle = (
        <Fragment>
            hello, <b>{user.username}</b>!
        </Fragment>
    );
    return (
        <Nav pullRight>
            <NavDropdown title={navTitle} id="user-menu">
                <LinkContainer to="/photos/new" exact>
                    <NavItem>Add new photo</NavItem>
                </LinkContainer>
                <LinkContainer to="/profile" exact>
                    <NavItem>Sign Up</NavItem>
                </LinkContainer>
                <MenuItem divider/>
                <MenuItem onClick={logout}>Logout</MenuItem>
            </NavDropdown>
        </Nav>
    )

};

export  default  UserName;