import {FETCH_PHOTOS_SUCCESS} from "../actions/actionTypes";


const initialState = {
    photos: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_PHOTOS_SUCCESS:
            console.log(action.photos);
            return {...state, photos: action.photos};
        default:
            return state;
    }
};

export default reducer;