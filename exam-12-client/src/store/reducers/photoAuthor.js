import {FETCH_AUTHOR_PHOTOS_SUCCESS} from "../actions/actionTypes";


const initialState = {
    photosAuthor: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_AUTHOR_PHOTOS_SUCCESS:
            console.log(action.photosAuthor);
            return {...state, photosAuthor: action.photosAuthor};
        default:
            return state;
    }
};

export default reducer;