import axios from '../../axios-api';
import {CREATE_PHOTO_SUCCESS, FETCH_PHOTOS_SUCCESS} from "./actionTypes";

export const fetchPhotosSuccess = photos => {
    return {type: FETCH_PHOTOS_SUCCESS, photos};
};

export const fetchPhotos = () => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        axios.get('/photos').then(
            response => {
                dispatch(fetchPhotosSuccess(response.data))
            }
        );
    }
};

export const createPhotoSuccess = () => {
    return {type: CREATE_PHOTO_SUCCESS};
};

export const createPhoto = photoData => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        console.log(photoData)
        return axios.post('/photos', photoData, {headers}).then(
            response => {
                dispatch(createPhotoSuccess());
            }

        );
    };
};
