import {DELETE_PHOTO_SUCCESS, FETCH_AUTHOR_PHOTOS_SUCCESS} from "./actionTypes";
import axios from '../../axios-api';

export const fetchPhotosAuthorSuccess = photosAuthor => {
    return {type: FETCH_AUTHOR_PHOTOS_SUCCESS, photosAuthor};
};

export const fetchPhotosAuthor = (id) => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        axios.get('/photos/' + id).then(
            response => {
                console.log(response.data)
                dispatch(fetchPhotosAuthorSuccess(response.data))
            }
        );
    }
};


export const deletePhotoSuccess = () => {
    return {type: DELETE_PHOTO_SUCCESS};
};

export const deletePhoto= (id) => {
    return (dispatch, getState) => {
        const headers = {
            'Token': getState().users.user.token
        };
        return axios.delete('/photos/' + id, {headers}).then(
            response => dispatch(deletePhotoSuccess())
        );
    };
};
