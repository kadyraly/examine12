import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import { PageHeader} from "react-bootstrap";
import PhotoList from "../../components/PhotoList/PhotoList";
import {fetchPhotos} from "../../store/actions/photos";


class Photo extends Component {


    componentDidMount() {
        this.props.onFetchPhotos();
    }


    render() {
        return (
            <Fragment>
                <PageHeader>
                    All photos
                </PageHeader>

                {this.props.photos.map(photo => (
                    <PhotoList
                        key={photo._id}
                        id={photo._id}
                        title={photo.title}
                        image={photo.image}
                        user={photo.user.username}
                        userId={photo.user._id}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        photos: state.photos.photos,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPhotos: () => dispatch(fetchPhotos())

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Photo);