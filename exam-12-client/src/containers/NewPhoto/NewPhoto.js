import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import PhotoForm from "../../components/PhotoForm/PhotoForm";
import {createPhoto} from "../../store/actions/photos";

class NewPhoto extends Component {
    createPhoto = photoData => {
        this.props.onPhotoCreated(photoData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New photo</PageHeader>
                <PhotoForm onSubmit={this.createPhoto}

                />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPhotoCreated: photoData => {
            return dispatch(createPhoto(photoData))
        }
    }
};



export default connect(null, mapDispatchToProps)(NewPhoto);