import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import {PageHeader} from "react-bootstrap";
import {deletePhoto, fetchPhotosAuthor} from "../../store/actions/photosAuthor";
import PhotoAuthorList from "../../components/PhotoAuthorList/PhotoAuthorList";


class PhotoAuthor extends Component {
    state = {
        deletedPhoto: null
    };

    componentDidMount() {
            this.props.onFetchPhotosAuthor(this.props.match.params.id);
        }


    componentDidUpdate(prevProps, prevState) {
        if(prevState.deletedPhoto !== this.state.deletedPhoto) {
            this.props.onFetchPhotosAuthor(this.props.match.params.id);
        }
    }

    onDeletePhoto = async (id) => {
        await this.props.deletePhoto(id);
        this.setState(() => {
            return {deletedPhoto: id}
        });

    };

    render() {
        return (
            <Fragment>
                <PageHeader>
                    {this.props.user && this.props.user.username}'s gallery
                </PageHeader>

                {this.props.photosAuthor.map(photoAuthor => (
                    <PhotoAuthorList
                        key={photoAuthor._id}
                        id={photoAuthor._id}
                        title={photoAuthor.title}
                        image={photoAuthor.image}
                        user={photoAuthor.user.username}
                        click={photoAuthor.user.username ? () => this.onDeletePhoto(photoAuthor._id) : null}

                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        photosAuthor: state.photosAuthor.photosAuthor,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPhotosAuthor: (id) => dispatch(fetchPhotosAuthor(id)),
        deletePhoto: (id) => dispatch(deletePhoto(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PhotoAuthor);